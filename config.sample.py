SERVER_ID = 1234
VERIFIED_ROLE_ID = 1234
ADMIN_ROLE_ID = 1234
BOT_CHANNEL_ID = 1234

EMBED_DEFAULT = 0x46b7e0 # blue
EMBED_SUCCESS = 0x10e82d # green
EMBED_WARNING = 0xd6e810 # yellow
EMBED_ERROR = 0xe81010 # red

VALID_ROLES = {"role1": 1234, "role2": 1234}

DISCORD_CLIENT_TOKEN = 'tokentokentoken'

GITLAB_TOKEN = 'tokentokentoken'
GITLAB_GROUP_ID = "123456"

EMAIL_SERVER = "something.com"
EMAIL_USERNAME = "something@example.com"
EMAIL_PASSWORD = "hunter2"
VALID_EMAIL_DOMAINS = ["example.com"]

# Below are set dynamically
guild = None
verified_role = None
bot_channel = None
main_path = None
client = None
